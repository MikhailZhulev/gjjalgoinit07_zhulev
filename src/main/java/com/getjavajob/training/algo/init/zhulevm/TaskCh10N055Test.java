package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N055.res;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 29.01.2016.
 */
public class TaskCh10N055Test {

    public static void main(String arg[]) {
        testNum();
    }

    public static void testNum() {
        assertEquals("TaskCh10N055Test.testNum: ", "2710", res(10000, 16));
    }

}
