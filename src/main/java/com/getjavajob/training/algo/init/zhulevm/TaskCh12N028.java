package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 04.02.2016.
 */
public class TaskCh12N028 {

    public static void main(String arg[]) {
        int[][] array = new int[11][11];
        int res[][] = moveArray(array);
        printArr(res);
    }

    public static int[][] moveArray(int x[][]) {
        int n = 1;
        int s = 0;
        while (s != (x.length - 1)) {

            //right action
            for (int i = s; i < 1 + s; i++) {
                for (int j = s; j < x[i].length - s; j++) {

                    x[i][j] = n;
                    n++;
                }
            }

            //down action
            for (int i = 1 + s; i < x.length - s; i++) {
                for (int j = (x[i].length - 1) - s; j < x[i].length - s; j++) {

                    x[i][j] = n;
                    n++;
                }
            }

            //left action
            for (int i = (x.length - 1) - s; i < x.length - s; i++) {
                for (int j = (x[i].length - 2) - s; j >= s; j--) {

                    x[i][j] = n;
                    n++;
                }
            }

            //up action
            for (int i = (x.length - 2) - s; i > s; i--) {
                for (int j = s; j < 1 + s; j++) {
                    x[i][j] = n;
                    n++;
                }
            }

            s++;
        }
        return x;
    }

    //method for print arrays
    public static void printArr(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                System.out.print("\t" + x[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
