package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N107 {

    public static void main(String arg[]) {
        String word = "Hararalll";
        String res = changeAO(word);
        System.out.println(res);
    }

    public static String changeAO(String s) {

        StringBuilder g = new StringBuilder(s);
        int index1 = s.indexOf('a');
        int index2 = s.lastIndexOf('o');

        // check have word symbol and A and O,
        // if dont have else index1 or index2 will be -1
        if (index1 != -1 && index2 != -1) {
            g.setCharAt(index1, 'o');
            g.setCharAt(index2, 'a');
        }

        String Res = g.toString();
        return Res;
    }
}
