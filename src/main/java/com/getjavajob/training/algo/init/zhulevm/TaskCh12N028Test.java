package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N028.moveArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 05.02.2016.
 */
public class TaskCh12N028Test {

    public static void main(String arg[]) {

        int[][] a = new int[][]{
                {1, 2, 3, 4, 5},

                {16, 17, 18, 19, 6},

                {15, 24, 25, 20, 7},

                {14, 23, 22, 21, 8},

                {13, 12, 11, 10, 9}
        };

        int s[][] = new int[5][5];
        testEq(a, s);
    }

    public static void testEq(int a[][], int s[][]) {
        assertEquals("TaskCh12N028Test.testEq: ", a, moveArray(s));
    }
}
