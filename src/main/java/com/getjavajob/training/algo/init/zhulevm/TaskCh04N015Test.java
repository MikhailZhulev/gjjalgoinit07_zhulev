package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh04N015.ageHuman;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh04N015Test {

    public static void main(String arg[]) {
        test1();
        test2();
        test3();
    }

    public static void test1() {
        assertEquals("TaskCh04N015Test.test 12-2014 6-1985 ", 29, ageHuman(2014, 12, 1985, 6));
    }

    public static void test2() {
        assertEquals("TaskCh04N015Test.test 5-2014 6-1985 ", 28, ageHuman(2014, 5, 1985, 6));
    }

    public static void test3() {
        assertEquals("TaskCh04N015Test.test 6-2014 6-1985 ", 29, ageHuman(2014, 6, 1985, 6));
    }
}
