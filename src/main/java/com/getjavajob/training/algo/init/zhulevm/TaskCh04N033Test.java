package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh04N033.even;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh04N033.odd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh04N033Test {

    public static void main(String[] args) {
        testEven();
        testOdd();
    }

    public static void testEven() {
        assertEquals("TaskCh04N033Test.test 10", true, even(10));
    }

    public static void testOdd() {
        assertEquals("TaskCh04N033Test.test 11", false, odd(8));
    }
}


