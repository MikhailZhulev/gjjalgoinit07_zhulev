package com.getjavajob.training.algo.init.zhulevm;

import java.util.Scanner;

/**
 * Created by ws_1 on 17.01.2016.
 */

public class TaskCh02N013 {

    public static void main(String atg[]) {

        Scanner s = new Scanner(System.in);

        //Input number
        System.out.println("Input num:");
        int num = s.nextInt();

        //Check have number on 3 numeral
        while (num > 999 || num < 100) {
            System.out.println("Not correct! Try again Input num:");
            num = s.nextInt();
        }
        System.out.println("Your num: " + theTotalNumber(num));
    }

    // Compute the total number
    static int theTotalNumber(int inputNum) {

        int oneF = (inputNum % 10);
        int twoF = ((inputNum % 100) - (inputNum % 10)) / 10;
        int threeF = ((inputNum % 1000) - (inputNum % 100)) / 100;

        int one = oneF * 100;
        int two = twoF * 10;
        int three = threeF * 1;

        int outputNum = one + two + three;

        return outputNum;

    }

}
