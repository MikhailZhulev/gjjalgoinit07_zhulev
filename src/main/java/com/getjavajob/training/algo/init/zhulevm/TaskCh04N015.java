package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh04N015 {

    public static void main(String arg[]) {
        int nowYear = 2016;      // today year
        int nowMonth = 3;        // today month
        int birthYear = 1989;      // birth year
        int birthMonth = 1;        // birth month
        int ageYears = ageHuman(nowYear, nowMonth, birthYear, birthMonth);
        int ageMonth = mon(nowYear, nowMonth, birthYear, birthMonth);
        System.out.println(ageYears + " year and " + ageMonth + " month");
    }
    // compute full month now
    static int nowTime(int a, int b) {
        int s = ((a * 12) + b);
        return s;
    }
    // compute full month of birth
    static int birthHuman(int year, int month) {
        int full = ((year * 12) + month);
        return full;
    }
    // compute Age human in years (difference month now and month of birth)
    static int ageHuman(int todayYear, int todayMonth, int birthYear, int birthMonth) {
        int result = ((nowTime(todayYear, todayMonth) - birthHuman(birthYear, birthMonth)) / 12);
        return result;
    }
    // compute rest in month (rest of month now and month of birth)
    static int mon(int todayYear, int todayMonth, int birthYear, int birthMonth) {
        int month = ((nowTime(todayYear, todayMonth) - birthHuman(birthYear, birthMonth)) % 12);
        return month;
    }
}