package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 31.01.2016.
 */
public class TaskCh11N158 {

    public static void main(String arg[]) {
        int startedArray[] = {4, 8, 4, 67, 32, 4, 6, 3, 3, 6};
        int[] res = array(startedArray);
        print(res);
    }

    public static int[] array(int[] x) {

        //find all duplicate elements and change on 0
        for (int i = 1; i < x.length; i++) {
            for (int j = 0; j < i; j++) {

                if (x[i] == x[j]) {
                    x[i] = 0;
                }
            }
        }

        //all elements have value 0 go to left on the arrays
        for (int i = 1; i < x.length; i++) {
            if (x[i] == 0) {
                for (int j = i; j < x.length - 1; j++) {
                    x[j] = x[j + 1];
                }
            }
        }
        return x;
    }

    //method for print arrays
    public static void print(int[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i]);
        }
    }

}
