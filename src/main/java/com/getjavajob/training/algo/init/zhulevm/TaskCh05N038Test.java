package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh05N038.farHouse;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh05N038.fullWay;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh05N038Test {
    public static void main(String arg[]) {
        testFullWay();
        testFarHouse();
    }

    public static void testFullWay() {
        assertEquals("TaskCh05N038Test.testFullWay 1 100: ", 5.187377517639621, fullWay(1, 100));
    }

    public static void testFarHouse() {
        assertEquals("TaskCh05N038Test.testFullWay 1 100: ", 0.688172179310195, farHouse(1, 100));
    }
}
