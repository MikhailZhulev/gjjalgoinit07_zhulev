package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh09N185.parenthesis;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N185Test {


    public static void main(String arg[]) {
        testParenthesis();
    }

    public static void testParenthesis() {
        assertEquals("TaskCh09N185Test.testParenthesis: ", "Well done! All good!", parenthesis("(((()()()()()()()())))"));
    }
}