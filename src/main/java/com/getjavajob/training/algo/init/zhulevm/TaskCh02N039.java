package com.getjavajob.training.algo.init.zhulevm;

import java.util.Scanner;

/**
 * Created by ws_1 on 17.01.2016.
 */
public class TaskCh02N039 {

    public static void main(String arg[]) {

        Scanner s = new Scanner(System.in);
        //Input TIME (hours,minute,seconds):
        //Input hours
        System.out.println("Input hours:");
        int hours = s.nextInt();
        // Check hours 0 < h <=24
        while (hours >= 24 || hours <= 0) {
            System.out.println("Input hours <24 and >0 :");
            hours = s.nextInt();
        }

        //Input minutes:
        System.out.println("Input minutes:");
        int minute = s.nextInt();
        //Check minutes 0 < m <=60
        while (minute >= 60 || minute < 0) {
            System.out.println("Input minutes <60 and <=0 :");
            minute = s.nextInt();
        }

        //Input seconds:
        System.out.println("Input seconds:");
        int seconds = s.nextInt();
        //Check seconds 0 < s <=60
        while (seconds >= 60 || seconds < 0) {
            System.out.println("Input seconds <60 and <=0 :");
            seconds = s.nextInt();
        }
        System.out.print(totalDegree(hours, minute, seconds) + " degree");
    }

    // method Compute the total Degree between start posishion
    // and posishion in time with hours,minute,second which transfer method
    static double totalDegree(int hours, int minute, int seconds) {
        int fullsec = seconds + (60 * minute) + (60 * 60 * hours);
        double degreesec = ((double) (360 / 12) / 60) / 60;
        double Degree = (fullsec * degreesec) % 360;  //rest of the division
        return Degree;

    }
}
