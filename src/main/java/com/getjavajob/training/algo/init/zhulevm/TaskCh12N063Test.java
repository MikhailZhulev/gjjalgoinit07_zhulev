package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N063.fullStudy;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 05.02.2016.
 */
public class TaskCh12N063Test {

    public static void main(String arg[]) {

        int[] fS = new int[]{65, 89, 98, 81, 79, 90, 84, 74, 91, 102, 90};

        int[][] sF = new int[][]{

                {16, 16, 17, 16},
                {22, 25, 18, 24},
                {18, 24, 27, 29},
                {23, 18, 19, 21},
                {22, 17, 15, 25},
                {21, 19, 26, 24},
                {22, 19, 27, 16},
                {16, 15, 19, 24},
                {23, 17, 26, 25},
                {28, 26, 23, 25},
                {29, 17, 28, 16},
        };

        testStudy(fS, sF);
    }

    public static void testStudy(int[] fS, int sF[][]) {
        assertEquals("TaskCh12N063Test.testStudy: ", fS, fullStudy(sF));
    }
}
