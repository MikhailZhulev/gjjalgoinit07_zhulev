package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 29.01.2016.
 */
public class TaskCh10N053 {

    public static void main(String arg[]) {
        int[] arr = new int[]{32, 34, 34, 0x10, 54};
        int p[] = arrNum(arr);

        for (int i = 0; i < p.length; i++) {
            System.out.print(p[i] + " ");
        }
    }

    //recursion num in reverse order
    public static int[] outNum(int[] x, int a, int b) {

        if (a == b) {
            return x;
        }

        if ((a + 1) == b) {
            int buf = x[a];
            x[a] = x[b];
            x[b] = buf;
            return x;
        }

        int buf = x[a];
        x[a] = x[b];
        x[b] = buf;

        int[] result = outNum(x, a + 1, b - 1);

        return result;
    }

    //wrappers for int[]
    public static int[] arrNum(int[] x) {
        int a = 0;
        int b = x.length - 1;
        return outNum(x, a, b);
    }
}
