package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N046.prog;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N046.sum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N46Test {
    public static void main(String arg[]) {

        testProg();

        testSum();
    }

    public static void testProg() {
        assertEquals("TaskCh10N046Test.testProg: ", 12, prog(3, 2, 3));
    }

    public static void testSum() {
        assertEquals("TaskCh10N046Test.testSum: ", 66, sum(2, 2, 6));
    }
}
