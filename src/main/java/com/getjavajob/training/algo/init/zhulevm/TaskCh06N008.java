package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh06N008 {

    public static void main(String arg[]) {
        int numN = 20000;
        int[] arr = arrayCalc(lastNum(numN));
        arrayPrint(arr);
    }
    // make array with need quantity value
    public static int[] arrayCalc(int Res) {
        int[] Array = new int[Res];
        for (int i = 0; i < Res; i++) {
            Array[i] = (int) Math.pow((i + 1), 2);
        }
        return Array;
    }

    // print our array
    public static void arrayPrint(int Array[]) {
        for (int i = 0; i < Array.length; i++) {
            System.out.println(Array[i]);
        }
    }

    // compute last Num
    public static int lastNum(int num) {
        double s = Math.sqrt(num);
        int w = (int) s;
        if (s == w) {
            w--;
        }
        return w;
    }
}
