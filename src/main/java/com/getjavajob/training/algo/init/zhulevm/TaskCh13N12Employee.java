package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 09.02.2016.
 */
public class TaskCh13N12Employee {

    private String Firstname;
    private String Lastname;
    private String Middlename;
    private String Adress;
    private int Monthbegin;
    private int Yearbegin;

    // Constructors class  TaskCh13N12Employee
    TaskCh13N12Employee() {
    }

    TaskCh13N12Employee(String Firstname, String Lastname, String Middlename, String Adress, int Monthbegin, int Yearsbegin) {
        this.Firstname = Firstname;
        this.Lastname = Lastname;
        this.Middlename = Middlename;
        this.Adress = Adress;
        this.Monthbegin = Monthbegin;
        this.Yearbegin = Yearsbegin;
    }

    TaskCh13N12Employee(String Firstname, String Lastname, String Adress, int Monthbegin, int Yearsbegin) {
        this.Firstname = Firstname;
        this.Lastname = Lastname;
        this.Middlename = "<Dont have MiddleName>";
        this.Adress = Adress;
        this.Monthbegin = Monthbegin;
        this.Yearbegin = Yearsbegin;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getMiddlename() {
        return Middlename;
    }

    public void setMiddlename(String middlename) {
        Middlename = middlename;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }

    public int getMonthbegin() {
        return Monthbegin;
    }

    public void setMonthbegin(int monthbegin) {
        Monthbegin = monthbegin;
    }

    public int getYearbegin() {
        return Yearbegin;
    }

    public void setYearbegin(int yearbegin) {
        Yearbegin = yearbegin;
    }

    //method returrn how mach years work employee
    public int workyear() {

        int Monthnow = 3;
        int Yearnow = 2016;

        int workyear = (((Yearnow * 12) + Monthnow) - ((Yearbegin * 12) + Monthbegin)) / 12;

        return workyear;
    }

}


