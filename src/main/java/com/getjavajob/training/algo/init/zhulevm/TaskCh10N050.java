package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N050 {

    public static void main(String arg[]) {
        int s = accerman(3, 5);
        System.out.println(s);
    }

    //compute method Accerman
    public static int accerman(int n, int m) {

        if (n == 0) {
            return (m + 1);
        }

        if (m == 0 && n != 0) {
            return accerman(n - 1, 1);
        }
        return accerman(n - 1, accerman(n, m - 1));
    }
}
