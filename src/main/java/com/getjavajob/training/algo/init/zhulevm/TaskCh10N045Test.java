package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N045.prog;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N045.sum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N045Test {

    public static void main(String arg[]) {
        testProg();
        testSum();
    }

    public static void testProg() {
        assertEquals("TaskCh10N045Test.testProg: ", 15, prog(5, 5, 3));
    }

    public static void testSum() {
        assertEquals("TaskCh10N045Test.testSum: ", 50, sum(5, 5, 4));
    }

}
