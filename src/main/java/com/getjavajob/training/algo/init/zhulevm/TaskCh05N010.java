package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 21.01.2016.
 */
public class TaskCh05N010 {

    public static void main(String arg[]) {
        double exchange = 75.15;  // current rate
        double[] array = new double[20];
        table(exchange, array);
        print(array);
    }

    public static double[] table(double x, double ar[]) {
        for (int i = 0; i < ar.length; i++) {
            ar[i] = (i + 1) * x;
        }
        return ar;
    }

    public static void print(double ar[]) {
        for (int i = 0; i < ar.length; i++) {
            System.out.println((i + 1) + "  " + String.format("%8.2f", ar[i]));
        }
    }
}
