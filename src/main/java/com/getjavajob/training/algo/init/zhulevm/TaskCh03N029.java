package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh03N029 {

    public static void main(String arg[]) {

        int x = 101;
        int y = 500;
        int z = 201;

        boolean resultA1 = solutionsTaskCh03N029a(x, y);
        boolean resultB1 = solutionsTaskCh03N029b(x, y);
        boolean resultC1 = solutionsTaskCh03N029c(x, y);
        boolean resultD1 = solutionsTaskCh03N029d(x, y, z);
        boolean resultE1 = solutionsTaskCh03N029e(x, y, z);
        boolean resultF1 = solutionsTaskCh03N029f(x, y, z);


        System.out.println("Task a)" + resultA1);
        System.out.println("Task b)" + resultB1);
        System.out.println("Task c)" + resultC1);
        System.out.println("Task d)" + resultD1);
        System.out.println("Task e)" + resultE1);
        System.out.println("Task f)" + resultF1);

    }

    static boolean solutionsTaskCh03N029a(int x, int y) {
        boolean r = (x % 2 == y % 2);
        return r;
    }

    static boolean solutionsTaskCh03N029b(int x, int y) {
        boolean r = (x < 20) ^ (y < 20);
        return r;
    }

    static boolean solutionsTaskCh03N029c(int x, int y) {
        boolean r = (x * y) == 0;
        return r;
    }

    static boolean solutionsTaskCh03N029d(int x, int y, int z) {
        boolean r = x < 0 && y < 0 && z < 0;
        return r;
    }

    static boolean solutionsTaskCh03N029e(int x, int y, int z) {
        boolean r = (x % 5 == 0) ^ (y % 5 == 0) ^ (z % 5 == 0);
        return r;
    }

    static boolean solutionsTaskCh03N029f(int x, int y, int z) {
        boolean r = (x > 100) ^ (y > 100) ^ (z > 100);
        return r;
    }
}
