package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh06N008.arrayCalc;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh06N008.lastNum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh06N008Test {

    public static void main(String arg[]) {
        int res = 9;
        int[] array = new int[res];

        for (int i = 0; i < res; i++) {
            array[i] = (int) Math.pow((i + 1), 2);
        }

        equalsArray(array);
    }

    public static void equalsArray(int Array[]) {
        assertEquals("TaskCh06N008Test.test Array 100", Array, arrayCalc(lastNum(100)));
    }
}
