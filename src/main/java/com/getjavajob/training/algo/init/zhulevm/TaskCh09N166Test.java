package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh09N166.change1on5;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N166Test {

    public static void main(String arg[]) {
        testChange1on5();
    }

    public static void testChange1on5() {
        assertEquals("TaskCh09N166Test.testChange1on5: ", "Five Two Three Four One ", change1on5("One Two Three Four Five"));
    }
}
