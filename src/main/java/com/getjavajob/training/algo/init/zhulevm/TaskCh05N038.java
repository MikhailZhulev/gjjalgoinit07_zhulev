package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh05N038 {

    public static void main(String arg[]) {
       // for example was step = 100
        double fullWayMan = fullWay(1, 100);
        double farHouseMan = farHouse(1, 100);

        System.out.println("Full Way:  " + fullWayMan);
        System.out.println("FarHouse:  " + farHouseMan);
    }

    //method compute full way which make husband
    public static double fullWay(double length, int steps) {
        double sum = 0;
        for (int i = 1; i <= steps; i++) {
            sum = sum + length / i;
        }
        return sum;
    }

    //method compute distance was make husband
    public static double farHouse(double length, int steps) {
        double full = 0;
        int min = -1;

        for (int i = 1; i <= steps; i++) {
            min = min * (-1);
            full = full + ((length / i) * min);
        }
        return full;
    }
}
