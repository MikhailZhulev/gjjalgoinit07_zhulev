package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 28.01.2016.
 */
public class TaskCh10N052 {

    public static void main(String arg[]) {

        String d = num(12345678);

        System.out.println(d);
    }

    //recursion num in reverse order
    public static String outNum(int t, String str) {

        int s;

        if (t < 10) {
            str = str + t;
            return str;
        } else {
            s = t % 10;
        }

        str = str + s;

        String result = outNum(t / 10, str);

        return result;
    }

    //wrappers for String
    public static String num(int x) {
        String f = "";
        return outNum(x, f);
    }
}
