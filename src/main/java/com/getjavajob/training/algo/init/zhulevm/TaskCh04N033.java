package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh04N033 {

    public static void main(String arg[]) {
        int num = 10;
        boolean result1 = even(num);
        boolean result2 = odd(num);
        System.out.println("Num Even?   " + result1);
        System.out.println("Num Odd?   " + result2);
    }

    // compute even number
    static boolean even(int num) {
        boolean result = (num % 2 == 0);
        return result;
    }
    // compute odd number
    static boolean odd(int num) {
        boolean result = (num % 2 == 1);
        return result;
    }
}