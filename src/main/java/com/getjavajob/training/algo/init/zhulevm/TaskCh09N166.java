package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N166 {

    public static void main(String arg[]) {
        String words = new String("One Two Three Four Five");
        String res = change1on5(words);
        System.out.println(res);
    }

    //change first word on last word
    public static String change1on5(String s) {
        String[] a = s.split(" ");
        String buf = a[a.length - 1];
        a[a.length - 1] = a[0];
        a[0] = buf;
        String h = "";

        for (int i = 0; i < a.length; i++) {
            h = h + a[i] + " ";
        }
        return h;
    }
}
