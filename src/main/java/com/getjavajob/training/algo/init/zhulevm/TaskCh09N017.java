package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N017 {

    public static void main(String arg[]) {

        String word = "Hellh";
        boolean result = eq(word);
        System.out.println(result);
    }

    //check first and last letter
    public static boolean eq(String s) {

        int len = s.length() - 1;
        char first = s.charAt(len);
        char end = s.charAt(0);
        boolean res = (first == end);
        return res;
    }
}
