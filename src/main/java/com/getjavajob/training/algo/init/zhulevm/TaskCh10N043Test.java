package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N043.all;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N043.sum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N043Test {

    public static void main(String arg[]) {
        testSum();
        testAll();
    }

    public static void testSum() {
        assertEquals("TaskCh10N043Test.testSum: ", 15, sum(555));
    }

    public static void testAll() {
        assertEquals("TaskCh10N043Test.testAll: ", 10, all(1234567890));

    }
}
