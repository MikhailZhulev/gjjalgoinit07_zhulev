package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 28.01.2016.
 */
public class TaskCh10N051 {

    public static void main(String arg[]) {
        proc1(5);
        System.out.println();
        proc2(5);
        System.out.println();
        proc3(5);

    }

    //result a
    public static int proc1(int n) {

        if (n > 0) {
            System.out.print(n);
        } else
            return 0;

        proc1(n - 1);
        return n;
    }

    //result b
    public static int proc2(int n) {

        int result = 0;

        if (n > 0) {
            result = proc2(n - 1);
            System.out.print(n);
        } else
            return 0;

        return result;
    }

    //result v
    public static int proc3(int n) {

        int result = 0;

        if (n > 0) {
            System.out.print(n);
            result = proc3(n - 1);
            System.out.print(n);

        } else
            return 0;

        return result;
    }
}
