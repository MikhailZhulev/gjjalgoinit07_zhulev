package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N036 {

    public static void main(String arg[]) {
        int NowMinute = 5;  // minute is real time
        String color = (colorLight(NowMinute));
        System.out.println(color);
    }
    // compute color light
    public static String colorLight(int x) {
        int Result = x % 5;
        String StatusLight = "Unknown";
        if (Result < 3) {
            StatusLight = "green";
        }
        if (Result >= 3) {
            StatusLight = "red";
        }
        return StatusLight;
    }
}
