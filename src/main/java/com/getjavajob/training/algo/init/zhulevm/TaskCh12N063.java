package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 05.02.2016.
 */
public class TaskCh12N063 {

    public static void main(String arg[]) {

        int array[][] = new int[11][4];

        //create array with random number
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                array[i][j] = (int) ((Math.random() * 15) + 15);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        int[] AllArray = fullStudy(array);

        printStudy(AllArray);
    }

    //method reurn sum study
    public static int[] fullStudy(int x[][]) {
        int[] sum = new int[x.length];

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                sum[i] = sum[i] + x[i][j];
            }
        }
        return sum;
    }

    //method print all Study
    public static void printStudy(int x[]) {
        for (int i = 0; i < x.length; i++) {
            System.out.print("In " + (i + 1) + " paralells study " + x[i] + " students.");
            System.out.println();
        }
    }

}
