package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N115 {

    public static void main(String arg[]) {
        int year = 1999;  // about this years will be full information
        String informationOfYear = info(year);
        System.out.println(informationOfYear);

    }

    public static String info(int x) {
        String real = result(x, color(calcColor(x)), animal(calcAnimal(x)));
        return real;
    }
    //Rest about years Animal when division on 12
    public static int calcAnimal(int now) {

        int point = 1984;
        do {
            point -= 60;
        } while ((now < point) && (point > 0));
        int animal = (now - point) % 12;
        return animal;
    }

    //Rest about years Color when division on 10

    public static int calcColor(int now) {
        int point = 1984;
        do {
            point -= 60;
        } while (now < point && point > 0);
        int color = (now - point) % 10;
        return color;
    }
    // method which return string about Years
    public static String result(int x, String a, String b) {
        String res = x + " Year: year " + a + " " + b;
        return res;
    }
    // compute Color of Years
    public static String color(int NumMon) {
        String realcolor;

        switch (NumMon) {
            case 0:
            case 1:
                realcolor = "Green";
                break;

            case 2:
            case 3:
                realcolor = "Red";
                break;

            case 4:
            case 5:
                realcolor = "Yellow";
                break;

            case 6:
            case 7:
                realcolor = "White";
                break;

            case 8:
            case 9:
                realcolor = "Black";
                break;

            default:
                realcolor = "unknown!";
        }
        return realcolor;
    }

    // compute Animal of years
    public static String animal(int NumMon) {
        String realanimal;

        switch (NumMon) {

            case 0:
                realanimal = "Rat";
                break;

            case 1:
                realanimal = "Cow";
                break;

            case 2:
                realanimal = "Tiger";
                break;

            case 3:
                realanimal = "Hare";
                break;

            case 4:
                realanimal = "Dragon";
                break;

            case 5:
                realanimal = "Snake";
                break;

            case 6:
                realanimal = "Hourse";
                break;

            case 7:
                realanimal = "Sheep";
                break;

            case 8:
                realanimal = "Monkey";
                break;

            case 9:
                realanimal = "Rooster";
                break;

            case 10:
                realanimal = "Dog";
                break;

            case 11:
                realanimal = "Pig";
                break;

            default:
                realanimal = "unknown!";
        }
        return realanimal;
    }
}



