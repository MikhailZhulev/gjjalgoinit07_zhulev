package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N048 {

    public static void main(String arg[]) {
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
            System.out.print(array[i] + " ");
        }

        System.out.println();
        int r = arr(array);
        System.out.println(r);
    }

    //compute max element on full array
    public static int maxArr(int[] array, int s, int max) {
        if (s == 0) {
            return max;
        }

        //compute max element in every steps
        if (array[s] > array[s - 1]) {
            if (max < array[s]) {
                max = array[s];
            }
        } else if (max < array[s - 1]) {
            max = array[s - 1];
        }

        //decreasing on 1
        s = s - 1;

        //recursion
        int result = maxArr(array, s, max);

        return result;
    }

    // wrappers method maxArr ,
    // in order to getting array and return array
    public static int arr(int[] Array) {
        int r = maxArr(Array, Array.length - 1, 0);
        return r;
    }
}