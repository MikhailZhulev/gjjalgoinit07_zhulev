package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh04N036.colorLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N036Test {
    public static void main(String arg[]) {
        test1();
        test2();
    }

    public static void test1() {
        assertEquals("TaskCh04N036Test.test 3", "red", colorLight(3));
    }

    public static void test2() {
        assertEquals("TaskCh04N036Test.test 5", "green", colorLight(5));
    }
}
