package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N045 {

    public static void main(String arg[]) {

        int firstNum = 5;
        int stepProg = 3;
        int fullStep = 6;

        int pro = prog(firstNum, stepProg, fullStep);
        int su = sum(firstNum, stepProg, fullStep);

        System.out.println(pro);
        System.out.println(su);

    }

    // function n-member progression
    public static int prog(int x, int y, int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return x;
        }

        int result = prog(x + y, y, n - 1);

        return result;
    }

    // function sum first n-member progression
    public static int sum(int x, int y, int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return x;
        }

        int sum = sum(x + y, y, n - 1) + x;
        return sum;
    }
}
