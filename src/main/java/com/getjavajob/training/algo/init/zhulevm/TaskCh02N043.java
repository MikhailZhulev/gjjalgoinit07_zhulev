package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 17.01.2016.
 */
public class TaskCh02N043 {

    public static void main(String arg[]) {
        int firstNum = 90;
        int secondNum = 10;
        int res = resultsDevision(firstNum, secondNum);
        System.out.println(res);
    }

    static int resultsDevision(int firstNum, int secondNum) {
        int result = ((firstNum % secondNum) * (secondNum % firstNum)) + 1;
        return result;
    }
}
