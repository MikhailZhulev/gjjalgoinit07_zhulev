package com.getjavajob.training.algo.init.zhulevm;


import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 31.01.2016.
 */
public class TaskCh12N023Test {

    public static void main(String arg[]) {

        int[][] a = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},

                {0, 1, 0, 0, 0, 1, 0},

                {0, 0, 1, 0, 1, 0, 0},

                {0, 0, 0, 1, 0, 0, 0},

                {0, 0, 1, 0, 1, 0, 0},

                {0, 1, 0, 0, 0, 1, 0},

                {1, 0, 0, 0, 0, 0, 1}
        };

        int[][] b = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},

                {0, 1, 0, 1, 0, 1, 0},

                {0, 0, 1, 1, 1, 0, 0},

                {1, 1, 1, 1, 1, 1, 1},

                {0, 0, 1, 1, 1, 0, 0},

                {0, 1, 0, 1, 0, 1, 0},

                {1, 0, 0, 1, 0, 0, 1}
        };

        int[][] c = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},

                {0, 1, 1, 1, 1, 1, 0},

                {0, 0, 1, 1, 1, 0, 0},

                {0, 0, 0, 1, 0, 0, 0},

                {0, 0, 1, 1, 1, 0, 0},

                {0, 1, 1, 1, 1, 1, 0},

                {1, 1, 1, 1, 1, 1, 1}
        };

        int[][] newArr1 = new int[7][7];
        int[][] newArr2 = new int[7][7];
        int[][] newArr3 = new int[7][7];

        testEq1(a, newArr1);
        testEq2(b, newArr2);
        testEq3(c, newArr3);
    }

    public static void testEq1(int a[][], int s[][]) {
        assertEquals("TaskCh12N023Test.testEq1: ", a, arr1(s));
    }

    public static void testEq2(int a[][], int s[][]) {
        assertEquals("TaskCh12N023Test.testEq2: ", a, arr2(s));
    }

    public static void testEq3(int a[][], int s[][]) {
        assertEquals("TaskCh12N023Test.testEq3: ", a, arr3(s));
    }
}
