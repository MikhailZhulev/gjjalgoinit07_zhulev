package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N043 {

    public static void main(String arg[]) {

        int sumNum = sum(34574343);
        int all = all(1234);

        System.out.println(sumNum);
        System.out.println(all);
    }

    //compute sum our number
    public static int sum(int a) {

        if (a < 10) {
            return a;
        }

        int ost = a % 10;
        int result = sum(a / 10) + ost;
        return result;
    }

    // compute full quantity symbol
    public static int all(int a) {

        if (a < 10) {
            return 1;
        }

        int result = all(a / 10) + 1;
        return result;
    }
}
