package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 02.02.2016.
 */
public class TaskCh12N025 {

    public static void main(String arg[]) {
        int[][] arrayA = new int[12][10];
        int[][] arrayB = new int[12][10];
        int[][] arrayV = new int[12][10];
        int[][] arrayG = new int[12][10];
        int[][] arrayD = new int[10][12];
        int[][] arrayE = new int[12][10];
        int[][] arrayZh = new int[12][10];
        int[][] arrayZ = new int[12][10];
        int[][] arrayI = new int[12][10];
        int[][] arrayK = new int[12][10];
        int[][] arrayL = new int[12][10];
        int[][] arrayM = new int[12][10];
        int[][] arrayN = new int[12][10];
        int[][] arrayO = new int[12][10];
        int[][] arrayP = new int[12][10];
        int[][] arrayR = new int[12][10];

        int a[][] = taskA(arrayA);
        int b[][] = taskB(arrayB);
        int v[][] = taskV(arrayV);
        int g[][] = taskG(arrayG);
        int d[][] = taskD(arrayD);
        int e[][] = taskE(arrayE);
        int zh[][] = taskZh(arrayZh);
        int z[][] = taskZ(arrayZ);
        int i[][] = taskI(arrayI);
        int k[][] = taskK(arrayK);
        int l[][] = taskL(arrayL);
        int m[][] = taskM(arrayM);
        int n[][] = taskN(arrayN);
        int o[][] = taskO(arrayO);
        int p[][] = taskP(arrayP);
        int r[][] = taskR(arrayR);

        printarr(a);
        System.out.println();
        printarr(b);
        System.out.println();
        printarr(v);
        System.out.println();
        printarr(g);
        System.out.println();
        printarr(d);
        System.out.println();
        printarr(e);
        System.out.println();
        printarr(zh);
        System.out.println();
        printarr(z);
        System.out.println();
        printarr(i);
        System.out.println();
        printarr(k);
        System.out.println();
        printarr(l);
        System.out.println();
        printarr(m);
        System.out.println();
        printarr(n);
        System.out.println();
        printarr(o);
        System.out.println();
        printarr(p);
        System.out.println();
        printarr(r);
        System.out.println();

    }

    //compute task A
    public static int[][] taskA(int[][] x) {
        int n = 1;
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                x[i][j] = n;
                n++;
            }
        }
        return x;
    }

    //compute task B
    public static int[][] taskB(int[][] x) {
        int n = 1;
        for (int i = 0; i < x[i].length; i++) {
            for (int j = 0; j < x.length; j++) {
                x[j][i] = n;
                n++;
            }
        }
        return x;
    }

    //compute task V
    public static int[][] taskV(int[][] x) {
        int n = 1;
        for (int i = 0; i < x.length; i++) {
            for (int j = x[i].length - 1; j >= 0; j--) {
                x[i][j] = n;
                n++;
            }
        }
        return x;
    }

    //compute task G
    public static int[][] taskG(int[][] x) {
        int n = 1;
        for (int i = 0; i < x[i].length; i++) {
            for (int j = x.length - 1; j >= 0; j--) {
                x[j][i] = n;
                n++;
            }
        }
        return x;
    }

    //compute task D
    public static int[][] taskD(int[][] x) {
        int n = 1;
        for (int i = 0; i < x.length; i++) {

            if (i % 2 == 0) {
                for (int j = 0; j < x[i].length; j++) {
                    x[i][j] = n;
                    n++;
                }
            } else {
                for (int j = x[i].length - 1; j >= 0; j--) {
                    x[i][j] = n;
                    n++;

                }
            }
        }
        return x;
    }

    //compute task E
    public static int[][] taskE(int[][] x) {
        int n = 1;
        for (int i = 0; i < x[i].length; i++) {

            if (i % 2 == 0) {

                for (int j = 0; j < x.length; j++) {
                    x[j][i] = n;
                    n++;
                }
            } else {

                for (int j = x.length - 1; j >= 0; j--) {
                    x[j][i] = n;
                    n++;
                }
            }
        }
        return x;
    }

    //compute task Zh
    public static int[][] taskZh(int[][] x) {
        int n = 1;
        for (int i = x.length - 1; i >= 0; i--) {

            for (int j = 0; j < x[i].length; j++) {
                x[i][j] = n;
                n++;
            }
        }
        return x;
    }

    //compute task Z
    public static int[][] taskZ(int[][] x) {
        int n = 1;
        int i = 0;
        for (i = x[i].length - 1; i >= 0; i--) {
            for (int j = 0; j < x.length; j++) {

                x[j][i] = n;
                n++;
            }
        }
        return x;
    }

    //compute task I
    public static int[][] taskI(int[][] x) {
        int n = 1;
        for (int i = x.length - 1; i >= 0; i--) {
            for (int j = x[i].length - 1; j >= 0; j--) {

                x[i][j] = n;
                n++;
            }
        }
        return x;
    }

    //compute task K
    public static int[][] taskK(int[][] x) {
        int n = 1;
        int i = 0;
        for (i = x[i].length - 1; i >= 0; i--) {
            for (int j = x.length - 1; j >= 0; j--) {

                x[j][i] = n;
                n++;
            }
        }
        return x;
    }

    //compute task L
    public static int[][] taskL(int[][] x) {
        int n = 1;
        for (int i = x.length - 1; i >= 0; i--) {

            if (i % 2 == 0) {
                for (int j = x[i].length - 1; j >= 0; j--) {
                    x[i][j] = n;
                    n++;
                }

            } else {
                for (int j = 0; j < x[i].length; j++) {
                    x[i][j] = n;
                    n++;
                }
            }
        }
        return x;

    }

    //compute task M
    public static int[][] taskM(int[][] x) {
        int n = 1;
        for (int i = 0; i < x.length; i++) {

            if (i % 2 == 0) {
                for (int j = x[i].length - 1; j >= 0; j--) {
                    x[i][j] = n;
                    n++;
                }
            } else {
                for (int j = 0; j < x[i].length; j++) {
                    x[i][j] = n;
                    n++;
                }
            }

        }
        return x;
    }

    //compute task N
    public static int[][] taskN(int[][] x) {
        int n = 1;
        int i = 0;
        for (i = x[i].length - 1; i >= 0; i--) {

            if (i % 2 == 0) {
                for (int j = x.length - 1; j >= 0; j--) {

                    x[j][i] = n;
                    n++;
                }

            } else {
                for (int j = 0; j < x.length; j++) {

                    x[j][i] = n;
                    n++;
                }
            }
        }
        return x;
    }

    //compute task O
    public static int[][] taskO(int[][] x) {
        int n = 1;

        for (int i = 0; i < x[i].length; i++) {

            if (i % 2 == 0) {
                for (int j = x.length - 1; j >= 0; j--) {

                    x[j][i] = n;
                    n++;
                }

            } else {
                for (int j = 0; j < x.length; j++) {

                    x[j][i] = n;
                    n++;
                }
            }
        }
        return x;

    }

    //compute task P
    public static int[][] taskP(int[][] x) {
        int n = 1;
        for (int i = x.length - 1; i >= 0; i--) {

            if (i % 2 == 0) {
                for (int j = 0; j < x[i].length; j++) {
                    x[i][j] = n;
                    n++;
                }

            } else {
                for (int j = x[i].length - 1; j >= 0; j--) {
                    x[i][j] = n;
                    n++;
                }
            }
        }
        return x;

    }

    //compute task R
    public static int[][] taskR(int[][] x) {
        int n = 1;
        int i = 0;
        for (i = x[i].length - 1; i >= 0; i--) {

            if (i % 2 == 0) {
                for (int j = 0; j < x.length; j++) {

                    x[j][i] = n;
                    n++;
                }

            } else {
                for (int j = x.length - 1; j >= 0; j--) {

                    x[j][i] = n;
                    n++;
                }
            }
        }
        return x;

    }

    //method for print arrays
    public static void printarr(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                System.out.print(x[i][j] + "  ");
            }
            System.out.println();
        }
    }
}