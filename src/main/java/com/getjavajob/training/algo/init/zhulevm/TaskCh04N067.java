package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N067 {

    public static void main(String arg[]) {
        int numDayOfWeek = 5;  // for example friday (num 5)
        String day = dayWeek(numDayOfWeek);
        System.out.println(day);
    }
    //compute num day of week ? weekend or workday
    public static String dayWeek(int k) {
        int res = (k % 7);
        String s = "Unknown";
        if (res == 0 || res == 6) {
            s = "Weekend";
        }
        if (res <= 5 && res > 0) {
            s = "Workday";
        }
        return s;
    }
}
