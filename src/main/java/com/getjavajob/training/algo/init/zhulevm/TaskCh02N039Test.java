package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh02N039.totalDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 17.01.2016.
 */
public class TaskCh02N039Test {

    public static void main(String arg[]) {
        degreeTest();
    }

    public static void degreeTest() {
        assertEquals("TaskCh02N039Test.test 3hours 30 min", 105.0, totalDegree(3, 30, 0));
    }
}
