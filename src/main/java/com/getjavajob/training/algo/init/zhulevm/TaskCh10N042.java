package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N042 {

    public static void main(String arg[]) {
        int degreeOfNun = a(2,5);
        System.out.println(degreeOfNun);
    }

    // method rate x num y
    public static int a(int x, int y) {

        if (y == 0) {
            return 1;
        }
        if (y == 1) {
            return x;
        }

        int result = x * a(x, (y - 1));
        return result;
    }
}
