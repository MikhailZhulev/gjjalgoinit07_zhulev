package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh09N022.half;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N022Test {

    public static void main(String arg[]) {
        char[] t = new char[]{'R', 'e', 's'};
        testHalf(t);
    }

    public static void testHalf(char t[]) {
        assertEquals("TaskCh09N022Test.testEquals Result: ", t, half("Result"));
    }
}
