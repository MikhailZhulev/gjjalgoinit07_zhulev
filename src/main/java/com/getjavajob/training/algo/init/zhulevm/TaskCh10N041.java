package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N041 {

    public static void main(String arg[]) {
        int factorial = fac(15);
        System.out.println(factorial);
    }

    //compute factorial t
    public static int fac(int t) {
        int result;
        if (t == 1) {
            return 1;
        }
        result = fac(t - 1) * t;
        return result;
    }

}
