package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 29.01.2016.
 */
public class TaskCh10N056 {

    public static void main(String arg[]) {
        int num = 15013;
        boolean easyNum = asks(num);
        System.out.println(num + " " + easyNum + " EasyNum ");
    }

    //method compute natural num
    public static boolean ask(int x, int n, int a) {

        if (x % 2 == 0) {
            return false;
        }

        if (n > a) {
            return true;
        }

        if (x % n != 0) {
            // System.out.print(n+" ");
            n = n + 2;

            if (x == n) {
                return true;
            }

            return ask(x, n, a);

        } else {
            return false;
        }
    }

    //wrappers for boolean
    public static boolean asks(int x) {
        return ask(x, 3, (x / 2));
    }
}
