package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 18.01.2016.
 */
public class TaskCh03N029Test {

    public static void main(String arg[]) {
        exercise1();
        exercise2();
        exercise3();
        exercise4();
        exercise5();
        exercise6();
    }

    public static void exercise1() {
        assertEquals("TaskCh03N029Test.test 101 500", false, solutionsTaskCh03N029a(101, 500));
    }

    public static void exercise2() {
        assertEquals("TaskCh03N029Test.test 101 500", false, solutionsTaskCh03N029b(101, 500));
    }

    public static void exercise3() {
        assertEquals("TaskCh03N029Test.test 101 500 ", false, solutionsTaskCh03N029c(101, 500));
    }

    public static void exercise4() {
        assertEquals("TaskCh03N029Test.test 101 500 201", false, solutionsTaskCh03N029d(101, 500, 201));
    }

    public static void exercise5() {
        assertEquals("TaskCh03N029Test.test 101 500 201", true, solutionsTaskCh03N029e(101, 500, 201));
    }

    public static void exercise6() {
        assertEquals("TaskCh03N029Test.test 101 500 201", true, solutionsTaskCh03N029f(101, 500, 201));
    }


}
