package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N106 {

    public static void main(String arg[]) {
        int mon = 10;  // for example november (num 10)
        String result = seasons(mon);
        System.out.println(result);
    }

    //compute seasons
    public static String seasons(int mon) {
        String season;

        switch (mon) {

            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;

            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;

            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;

            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;

            default:
                season = "unknown month";
        }
        return season;
    }
}
