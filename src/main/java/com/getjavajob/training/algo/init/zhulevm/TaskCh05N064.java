package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh05N064 {

    public static void main(String arg[]) {
        // people living in each district
        int[] people = new int[]{1000, 11000, 2120, 2130, 2140, 2150, 2160, 2170, 2180, 2190, 2200, 2210};
       // square each district
        int[] sq = new int[]{100, 100, 200, 300, 400, 500, 600, 700, 800, 900, 500, 100};
        // share density
        double FullDensity = shareDensity(sq, people);
        System.out.println(FullDensity);
    }

    public static double shareDensity(int[] sq, int[] people) {
        double[] density = new double[12];
        // density evry district
        for (int i = 0; i < density.length; i++) {
            density[i] = (double) people[i] / sq[i];
        }

        double sumDensity = 0;
        // sum density all district
        for (int i = 0; i < density.length; i++) {
            sumDensity = sumDensity + density[i];
        }
        // middle density
        double res = sumDensity / 12.0;
        return res;
    }
}
