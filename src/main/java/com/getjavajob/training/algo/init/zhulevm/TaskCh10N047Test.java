package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N047.fib;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N047Test {

    public static void main(String arg[]) {
        testFib();
    }

    public static void testFib() {
        assertEquals("TaskCh10N047Test.testFib: ", 8, fib(6));
    }
}
