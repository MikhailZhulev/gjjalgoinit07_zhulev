package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh05N010.table;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 21.01.2016.
 */
public class TaskCh05N010Test {
    public static void main(String arg[]) {
        double[] array = new double[20];
        double[] expected = new double[20];
        for (int i = 0; i < expected.length; i++) {
            expected[i] = (i + 1) * 75.15;
        }
        test1(expected, array);
    }

    public static void test1(double expected[], double array[]) {
        assertEquals("TaskCh05N010Test.test equals", expected, table(75.15, array));
    }
}



