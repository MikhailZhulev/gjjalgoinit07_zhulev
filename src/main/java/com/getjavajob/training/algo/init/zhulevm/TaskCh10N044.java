package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N044 {

    public static void main(String arg[]) {
        int digitalRadical = sum(99799994);
        System.out.println(digitalRadical);

    }

    // compute numeral root
    public static int sum(int a) {

        if (a < 10) {
            return a;
        }

        int ost = a % 10;
        int result = sum(a / 10) + ost;

        if (result < 10) {
            return result;
        }

        int t = sum(result);
        return t;
    }
}
