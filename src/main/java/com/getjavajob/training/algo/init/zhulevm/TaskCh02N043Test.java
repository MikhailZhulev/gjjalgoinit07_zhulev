package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh02N043.resultsDevision;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 17.01.2016.
 */
public class TaskCh02N043Test {

    public static void main(String[] args) {
        testResidue();
    }

    public static void testResidue() {
        assertEquals("TaskCh02N043Test.test 100 10", 1, resultsDevision(100, 10));
        assertEquals("TaskCh02N043Test.test 99 20", true, resultsDevision(99, 20) != 1);
    }
}
