package com.getjavajob.training.algo.init.zhulevm;

import java.util.Scanner;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh06N087 {

    public static void main(String arg[]) {
        System.out.println("Welcome to the Match!");
        play();
    }

    public static void play() {
        //create 2 object (2 teams)
        Game team1 = new Game();
        Game team2 = new Game();

        Scanner t = new Scanner(System.in);

        System.out.println("Score: " + team1.sum + "  " + team2.sum);

        //This algorithm select team and plus point,
        // if s = 0 will be finish game and started
        // method class Game sum inside method res.
        int s = 1;
        while (s != 0) {
            System.out.println("Input № team(1/2) :  ");
            s = t.nextInt();

            if (s == 1) {
                System.out.println("Input point(1,2,3):  ");
                team1.sum();
                System.out.println("Score: " + team1.sum + "  " + team2.sum);

            }
            if (s == 2) {
                System.out.println("Input point(1,2,3):  ");
                team2.sum();
                System.out.println("Score: " + team1.sum + "  " + team2.sum);
            }
            if (s > 2 || s < 0) {
                System.out.println("Insert again 1 or 2 nummer team! ");
            }

            if (s == 0) {
                System.out.println(res(team1.sum, team2.sum));
            }
        }
    }

    // method compute which team victory
    public static String res(int x, int y) {
        String d = null;
        if (x > y) {
            d = "Win 1 team";
        }
        if (x < y) {
            d = "Win 2 team";
        }
        if (x == y) {
            d = "Win Friends";
        }

        System.out.println("Score: " + x + " " + y);
        return d;
    }
}

// in this object class we use method sum()
// and compute sum points every objects (teams)
class Game {

    int sum = 0;

    public int sum() {
        Scanner t = new Scanner(System.in);
        int x = t.nextInt();
        while (x > 3 || x <= 0) {
            System.out.println("Input point for 1 do 3 ! :");
            x = t.nextInt();
        }
        sum = sum + x;
        return sum;
    }
}