package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N022 {

    public static void main(String arg[]) {
        String word = "Result";
        char[] result = half(word);
        System.out.println(result);
    }

    //return array chars first half word
    public static char[] half(String s) {

        int all = (s.length()) / 2;
        int start = 0;
        int end = all;

        char buf[] = new char[end - start];
        s.getChars(start, end, buf, 0);

        return buf;
    }
}
