package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 01.02.2016.
 */
public class TaskCh12N024 {

    public static void main(String arg[]) {
        int array[][] = new int[6][6];
        int[][] arr1 =resArr1(array);
        int[][] arr2 =resArr1(array);
        printArr(arr1);
        printArr(arr2);
    }

    public static int[][] resArr1(int[][] x) {

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                x[i][j] = 1;
                if (i > 0 && j > 0) {
                    x[i][j] = x[i][j - 1] + x[i - 1][j];
                }
            }
        }
        return x;
    }


    public static int[][] resArr2(int[][] x) {

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                x[i][j] = Math.abs(j - i) + 1;

                if (i > 0) {
                    x[i][x[j].length - 1] = x[i - 1][0];
                }

                if (i > 0 && j > 0 && j < x[i].length - 1) {
                    x[i][j] = x[i - 1][j + 1];
                }
            }
        }
        return x;
    }

    //method for print arrays
    public static void printArr(int[][] x) {

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                System.out.print(x[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
