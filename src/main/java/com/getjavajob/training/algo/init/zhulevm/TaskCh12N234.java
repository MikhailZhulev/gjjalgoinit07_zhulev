package com.getjavajob.training.algo.init.zhulevm;

import java.util.Arrays;

/**
 * Created by ws_1 on 07.02.2016.
 */
public class TaskCh12N234 {

    public static void main(String arg[]) {

        int array[][] = new int[7][7];

        fullArrRandom(array);

        //System.out.println(Arrays.deepToString(Array));
        array = deleteColumnArray(array, 3);
        array = deleteStringArray(array, 3);

        //System.out.println(Arrays.deepToString(Array));
        printArr(array);
    }

    //method return array with delete string
    public static int[][] deleteStringArray(int array[][], int x) {

        int copyArray[][] = new int[7][7];
        System.arraycopy(array, x, copyArray, x - 1, array.length - x);
        System.arraycopy(array, 0, copyArray, 0, x - 1);
        return copyArray;
    }

    //method return array with delete column
    public static int[][] deleteColumnArray(int array[][], int x) {

        int copyArray[][] = new int[7][7];

        for (int i = 0; i < array.length; i++) {
            System.arraycopy(array[i], x, copyArray[i], x - 1, array[i].length - x);
            System.arraycopy(array[i], 0, copyArray[i], 0, x - 1);

        }
        return copyArray;
    }

    //method print array
    public static void printArr(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                System.out.print("\t" + x[i][j] + "\t");
            }
            System.out.println();
        }
    }

   //method make random int array
    public static int[][] fullArrRandom(int[][] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {

                array[i][j] = (int) ((Math.random() * 15) + 15);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        return array;
    }

}
