package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N049 {

    public static void main(String arg[]) {
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
            System.out.print(array[i] + " ");
        }

        System.out.println();
        int r = arr(array);
        System.out.println(r);
    }

    //compute max index on full array
    public static int maxArr(int[] array, int s, int max, int index) {

        if (s == 0) {
            return (index + 1);
        }

        if (array[s] > array[s - 1]) {

            if (max < array[s]) {
                max = array[s];
                index = s;
            }

        } else if (max < array[s - 1]) {
            max = array[s - 1];
            index = s - 1;
        }

        // for find last max index (recursion going from right to left)
        if (max == array[s]) {
            index = s;
        }

        //decreasing on 1
        s = s - 1;

        int result = maxArr(array, s, max, index);
        return result;
    }

    //compute max element on full array
    public static int arr(int[] Array) {
        int r = maxArr(Array, Array.length - 1, 0, 0);
        return r;
    }
}
