package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N047 {

    public static void main(String arg[]) {
        int fibonachi = fib(6);
        System.out.println(fibonachi);
    }

    // method Fibonachi
    public static int fib(int n) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 1;
        }
        int result = fib(n - 1) + fib(n - 2);
        return result;
    }
}
