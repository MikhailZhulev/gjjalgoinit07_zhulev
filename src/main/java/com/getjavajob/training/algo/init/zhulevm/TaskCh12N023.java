package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 31.01.2016.
 */
public class TaskCh12N023 {

    public static void main(String arg[]) {

        int[][] array1 = new int[7][7];
        int[][] array2 = new int[7][7];
        int[][] array3 = new int[7][7];  // create different Arrays, because if Array will be once, will be bad.

        int[][] nArr1 = arr1(array1);
        int[][] nArr2 = arr2(array2);
        int[][] nArr3 = arr3(array3);

        printArr(nArr1);
        System.out.println();
        printArr(nArr2);
        System.out.println();
        printArr(nArr3);
    }

    //method for print arrays
    public static void printArr(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                System.out.print(x[i][j] + "  ");
            }
            System.out.println();
        }
    }

    public static int[][] arr1(int[][] x) {

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {

                if ((j == i) || j == ((x[i].length - 1) - i)) {

                    x[i][j] = 1;
                }
            }
        }
        return x;
    }

    public static int[][] arr2(int[][] x) {

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {

                if (j == (0 + i) || j == ((x[i].length - 1) - i) || i == ((x[i].length - 1) - i) || j == ((x[i].length - 1) - j)) {
                    x[i][j] = 1;
                }
            }
        }
        return x;
    }

    public static int[][] arr3(int[][] x) {
        int n = 0;
        int d = 0;
        for (int i = 0; i < x.length; i++) {
            for (int j = d; j < (x[i].length - d); j++) {
                x[i][j] = 1;
            }
            n++;
            d = (x.length / 2) - Math.abs((x.length / 2) - n);
        }
        return x;
    }
}
