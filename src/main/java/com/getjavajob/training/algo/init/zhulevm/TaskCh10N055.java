package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 29.01.2016.
 */
public class TaskCh10N055 {

    public static void main(String arg[]) {
        String num = res(146765, 16);
        System.out.println(num);
    }

    public static String Change(String s, int x, int y, int o) {

        if (x >= y) {

            o = x % y;
            if (o < 10) {
                s = o + s;
            }
            if (o == 10) {
                s = "A" + s;
            }
            if (o == 11) {
                s = "B" + s;
            }
            if (o == 12) {
                s = "C" + s;
            }
            if (o == 13) {
                s = "D" + s;
            }
            if (o == 14) {
                s = "E" + s;
            }
            if (o == 15) {
                s = "F" + s;
            }
            //System.out.print(s + " ");
            return Change(s, x / y, y, o);

        } else {

            if (x < 10) {
                s = x + s;
            }
            if (x == 10) {
                s = "A" + s;
            }
            if (x == 11) {
                s = "B" + s;
            }
            if (x == 12) {
                s = "C" + s;
            }
            if (x == 13) {
                s = "D" + s;
            }
            if (x == 14) {
                s = "E" + s;
            }
            if (x == 15) {
                s = "F" + s;
            }
            //System.out.println(s);
            return s;
        }
    }

    //wrappers for num
    public static String res(int x, int y) {
       if( y>=2 && y<=16) {
           return Change("", x, y, 0);
       }else{
           return "Dont correct rate!";
       }
    }

}

