package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N041.fac;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 27.01.2016.
 */
public class TaskCh10N041Test {

    public static void main(String arg[]) {
        testFac();
    }

    public static void testFac() {
        assertEquals("TaskCh10N041Test.testFac: ", 2004310016, fac(15));
    }
}
