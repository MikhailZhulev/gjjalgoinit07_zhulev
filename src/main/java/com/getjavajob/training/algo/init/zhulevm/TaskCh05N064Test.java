package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh05N064.shareDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh05N064Test {

    public static void main(String arg[]) {

        int[] people = new int[]{1000, 11000, 2120, 2130, 2140, 2150, 2160, 2170, 2180, 2190, 2200, 2210};
        int[] sq = new int[]{100, 100, 200, 300, 400, 500, 600, 700, 800, 900, 500, 100};

        testDensity(sq, people);
    }

    public static void testDensity(int[] sq, int[] people) {
        assertEquals("TaskCh05N064Test.test Density", 15.475694444444443, shareDensity(sq, people));
    }
}


