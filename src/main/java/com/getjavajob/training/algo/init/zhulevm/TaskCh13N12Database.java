package com.getjavajob.training.algo.init.zhulevm;

import java.util.ArrayList;

/**
 * Created by ws_1 on 11.02.2016.
 */
class TaskCh13N12Database {

    static ArrayList<TaskCh13N12Employee> list = new ArrayList<>();

    //this methos create list with 20 employee
    static ArrayList List() {

        TaskCh13N12Employee employee1 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee2 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee3 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee4 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee5 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee6 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee7 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee8 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee9 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee10 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee11 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee12 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee13 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee14 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee15 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee16 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee17 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee18 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee19 = new TaskCh13N12Employee();
        TaskCh13N12Employee employee20 = new TaskCh13N12Employee();

        employee1.setFirstname("Иван");
        employee1.setLastname("Левин");
        employee1.setMiddlename("Сергеевич");
        employee1.setAdress("Moskow");
        employee1.setMonthbegin(5);
        employee1.setYearbegin(2000);
        employee1.workyear();

        employee2.setFirstname("Сергей");
        employee2.setLastname("Кучерявено");
        employee2.setMiddlename("Михайлович");
        employee2.setAdress("Moskow");
        employee2.setMonthbegin(5);
        employee2.setYearbegin(2010);
        employee2.workyear();

        employee3.setFirstname("Михаил");
        employee3.setLastname("Жулёв");
        employee3.setMiddlename("Михайлович");
        employee3.setAdress("Domododovo");
        employee3.setMonthbegin(1);
        employee3.setYearbegin(2011);
        employee3.workyear();

        employee4.setFirstname("Николай");
        employee4.setLastname("Амельчев");
        employee4.setMiddlename("Михайлович");
        employee4.setAdress("Domododovo");
        employee4.setMonthbegin(2);
        employee4.setYearbegin(2012);
        employee4.workyear();

        employee5.setFirstname("Эдуард");
        employee5.setLastname("Галейс");
        employee5.setMiddlename("Михайлович");
        employee5.setAdress("Domododovo");
        employee5.setMonthbegin(5);
        employee5.setYearbegin(2011);
        employee5.workyear();

        employee6.setFirstname("Андрей");
        employee6.setLastname("Бесчастный");
        employee6.setMiddlename("Андреевич");
        employee6.setAdress("Domododovo");
        employee6.setMonthbegin(5);
        employee6.setYearbegin(2012);
        employee6.workyear();

        employee7.setFirstname("Артём");
        employee7.setLastname("Ермоченко");
        employee7.setMiddlename("Андреевич");
        employee7.setAdress("Domododovo");
        employee7.setMonthbegin(3);
        employee7.setYearbegin(2011);
        employee7.workyear();

        employee8.setFirstname("Семён");
        employee8.setLastname("Самелов");
        employee8.setMiddlename("Андреевич");
        employee8.setAdress("Domododovo");
        employee8.setMonthbegin(4);
        employee8.setYearbegin(2004);
        employee8.workyear();

        employee9.setFirstname("Юрий");
        employee9.setLastname("Погобатько");
        employee9.setMiddlename("Андреевич");
        employee9.setAdress("Domododovo");
        employee9.setMonthbegin(6);
        employee9.setYearbegin(2006);
        employee9.workyear();

        employee10.setFirstname("Владимир");
        employee10.setLastname("Калугин");
        employee10.setMiddlename("Андреевич");
        employee10.setAdress("Domododovo");
        employee10.setMonthbegin(7);
        employee10.setYearbegin(2008);
        employee10.workyear();

        employee11.setFirstname("Роман");
        employee11.setLastname("Заиграев");
        employee11.setMiddlename("Николаевич");
        employee11.setAdress("Domododovo");
        employee11.setMonthbegin(8);
        employee11.setYearbegin(2009);
        employee11.workyear();

        employee12.setFirstname("Дмитрий");
        employee12.setLastname("Непомнящий");
        employee12.setMiddlename("Николаевич");
        employee12.setAdress("Moskow");
        employee12.setMonthbegin(9);
        employee12.setYearbegin(2003);
        employee12.workyear();

        employee13.setFirstname("Марк");
        employee13.setLastname("Белый");
        employee13.setMiddlename("Николаевич");
        employee13.setAdress("Moskow");
        employee13.setMonthbegin(10);
        employee13.setYearbegin(2006);
        employee13.workyear();

        employee14.setFirstname("Антон");
        employee14.setLastname("Васин");
        employee14.setMiddlename("Николаевич");
        employee14.setAdress("Moskow");
        employee14.setMonthbegin(11);
        employee14.setYearbegin(2007);
        employee14.workyear();

        employee15.setFirstname("Василий");
        employee15.setLastname("Ермоченко");
        employee15.setMiddlename("Эдуардович");
        employee15.setAdress("Moskow");
        employee15.setMonthbegin(11);
        employee15.setYearbegin(2004);
        employee15.workyear();

        employee16.setFirstname("Денис");
        employee16.setLastname("Ковалёв");
        employee16.setMiddlename("Эдуардович");
        employee16.setAdress("Moskow");
        employee16.setMonthbegin(5);
        employee16.setYearbegin(2008);
        employee16.workyear();

        employee17.setFirstname("Артём");
        employee17.setLastname("Ермоченко");
        employee17.setMiddlename("Эдуардович");
        employee17.setAdress("Moskow");
        employee17.setMonthbegin(8);
        employee17.setYearbegin(2012);
        employee17.workyear();

        employee18.setFirstname("Генадий");
        employee18.setLastname("Горич");
        employee18.setMiddlename("иванович");
        employee18.setAdress("Moskow");
        employee18.setMonthbegin(7);
        employee18.setYearbegin(2001);
        employee18.workyear();

        employee19.setFirstname("Кирилл");
        employee19.setLastname("Иванов");
        employee19.setMiddlename("Семёнович");
        employee19.setAdress("Moskow");
        employee19.setMonthbegin(4);
        employee19.setYearbegin(2004);
        employee19.workyear();

        employee20.setFirstname("Леонид");
        employee20.setLastname("Лекарь");
        employee20.setMiddlename("Иванович");
        employee20.setAdress("Moskow");
        employee20.setMonthbegin(11);
        employee20.setYearbegin(2005);
        employee20.workyear();

        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        list.add(employee4);
        list.add(employee5);
        list.add(employee6);
        list.add(employee7);
        list.add(employee8);
        list.add(employee9);
        list.add(employee10);
        list.add(employee11);
        list.add(employee12);
        list.add(employee13);
        list.add(employee14);
        list.add(employee15);
        list.add(employee16);
        list.add(employee17);
        list.add(employee18);
        list.add(employee19);
        list.add(employee20);

        return list;
    }

    static ArrayList serviceTimeMore(int n) {

        ArrayList<TaskCh13N12Employee> list = TaskCh13N12Database.list;
        ArrayList<TaskCh13N12Employee> listServiseTime = new ArrayList<>();

        for (TaskCh13N12Employee employee : list) {

            if (employee.workyear() > n) {
                System.out.println(employee.getFirstname() + "\t" + "\t" + employee.getLastname());
                listServiseTime.add(employee);
            }
        }
        return listServiseTime;    // we can nothing return and in Main not will be have access in Object
    }

    //method find employee with string n and return Name employee
    static ArrayList findEmployee(String n) {

        ArrayList<TaskCh13N12Employee> list = TaskCh13N12Database.list;
        ArrayList<TaskCh13N12Employee> listFindString = new ArrayList<>();

        for (TaskCh13N12Employee employee : list) {

            if (
                    ((employee.getFirstname().toLowerCase()).indexOf(n.toLowerCase())) != -1 ||

                            ((employee.getLastname().toLowerCase()).indexOf(n.toLowerCase())) != -1 ||

                            ((employee.getMiddlename().toLowerCase()).indexOf(n.toLowerCase())) != -1
                    ) {
                System.out.println(employee.getFirstname() + "\t" + "\t" + employee.getLastname() + "\t" + "\t" + employee.getMiddlename());

                listFindString.add(employee);
            }

        }
        return listFindString;
    }

    // add new employee in list Firstname, Lastname, Middlename, adress month and years
    static void newEmployee(String Firstname, String Lastname, String Middlename, String adress, int x, int y) {
        TaskCh13N12Employee employe = new TaskCh13N12Employee(Firstname, Lastname, Middlename, adress, x, y);
        list.add(employe);

    }

    // add new employee in list Firstname, Lastname,  adress month and years
    static void newEmployee(String Firstname, String Lastname, String adress, int x, int y) {
        TaskCh13N12Employee employe = new TaskCh13N12Employee(Firstname, Lastname, adress, x, y);
        list.add(employe);
    }

    //print all employee
    static void PrintAll() {

        System.out.printf("%16s", "Firstname");
        System.out.print("\t");
        System.out.printf("%15s", "Lastname");
        System.out.print("\t");
        System.out.printf("%23s", "Middlename");
        System.out.print("\t");
        System.out.printf("%12s", "Adress");
        System.out.print("\t");
        System.out.printf("%11s", "Monthbegin");
        System.out.print("\t");
        System.out.printf("%11s", "Yearbegin");
        System.out.print("\t");
        System.out.printf("%11s", "Workyear");
        System.out.println();
        System.out.println("----------------------------------------------------------------------------------------------------------------------------");

        for (TaskCh13N12Employee employee : list) {

            System.out.printf("%16s", employee.getFirstname());
            System.out.print("\t");
            System.out.printf("%15s", employee.getLastname());
            System.out.print("\t");
            System.out.printf("%23s", employee.getMiddlename());
            System.out.print("\t");
            System.out.printf("%12s", employee.getAdress());
            System.out.print("\t");
            System.out.printf("%11d", employee.getMonthbegin());
            System.out.print("\t");
            System.out.printf("%11d", employee.getYearbegin());
            System.out.print("\t");
            System.out.printf("%9d", employee.workyear());
            System.out.println();
            System.out.println();
        }
    }
}
