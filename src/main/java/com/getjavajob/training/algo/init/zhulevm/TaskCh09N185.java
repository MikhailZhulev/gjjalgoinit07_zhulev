package com.getjavajob.training.algo.init.zhulevm;


/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N185 {

    public static void main(String arg[]) {
        String words = "((()))dgdffg(()()(())";
        String res = parenthesis(words);
        System.out.println(res);
    }


    public static String parenthesis(String s) {
        String m = null;
        StringBuilder g = new StringBuilder(s);

        int index3;
        int length = s.length();
        int tre = 0;

        for (int i = 0; i < length; i++) {
            //System.out.println(s);      // if remove comments will be see how work programmers

            int index1 = s.lastIndexOf('(');

            if (s.contains("(") != true && s.contains(")") != true) {
                tre = 3;
                break;
            }

            if (s.contains("(") != true && s.contains(")")) {
                m = "Without open parenthesis " + (s.indexOf(')') + 1);
                tre = 1;
                break;
            }

            //create new array where first "("
            char buf[] = new char[length - index1];
            s.getChars(index1 + 1, length, buf, 0);
            String s1 = new String(buf);

            index3 = s1.indexOf(')');

            //change pair symbol "(",")" in right order on symbol 1 and 0 respectively
            if (s1.contains(")")) {
                g.setCharAt(index1, '0');
                g.setCharAt(index1 + index3 + 1, '1');

                s = new String(g);

            } else {
                m = "Error! Without close parenthesis  " + (index1 + 1);
                tre = 2;
                break;
            }

            tre = 3;
        }

        // if in our words the same symbol "(" and ")" , and they go in right order
        if (tre == 3) {
            m = "Well done! All good!";
        }
        return m;
    }
}



