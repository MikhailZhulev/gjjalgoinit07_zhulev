package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 31.01.2016.
 */
public class TaskCh11N245 {

    public static void main(String arg[]) {
        int firstArray[] = {4, -8, 4, -67, 32, -4, 6, -3, 3, -6};
        int[] res = sort(firstArray);
        print(res);
    }

    // sorted negative number first and after all other
    public static int[] sort(int[] x) {

        int[] B = new int[x.length];
        int y = 0;
        int z = x.length - 1;

        for (int i = 0; i < x.length; i++) {

            //assign first value negative num
            if (x[i] < 0) {
                B[y] = x[i];
                y++;
            }

            //assign last value 0 or positive num
            if (x[i] >= 0) {
                B[z] = x[i];
                z--;
            }
        }
        return B;
    }

    //method for print arrays
    public static void print(int[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
    }
}