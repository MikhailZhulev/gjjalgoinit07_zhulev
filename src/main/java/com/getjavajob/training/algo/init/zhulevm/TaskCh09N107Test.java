package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh09N107.changeAO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 25.01.2016.
 */
public class TaskCh09N107Test {

    public static void main(String arg[]) {
        testChangeAO();

    }

    public static void testChangeAO() {
        assertEquals("TaskCh09N107Test.testChangeAO: ", "Horaralolola", changeAO("Hararalololo"));
    }
}
