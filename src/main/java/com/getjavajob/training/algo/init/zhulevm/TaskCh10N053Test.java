package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh10N053.arrNum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 29.01.2016.
 */
public class TaskCh10N053Test {

    public static void main(String arg[]) {

        int[] arrs = new int[]{32, 34, 34, 76, 54};
        int[] arrayActual = new int[]{54, 76, 34, 34, 32};

        ANum(arrs, arrayActual);
    }

    public static void ANum(int[] arr, int[] arrayactual) {
        assertEquals("TaskCh10N053Test.testNum: ", arrayactual, arrNum(arr));
    }
}
