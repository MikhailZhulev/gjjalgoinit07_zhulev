package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N015 {

    public static void main(String arg[]) {
        String word = "Password";
        char symbol = num(word, 4);
        System.out.println(symbol);
    }

    // find symbol № r-1 and return him
    public static char num(String s, int r) {
        char n = s.charAt(r - 1);
        return n;
    }
}
