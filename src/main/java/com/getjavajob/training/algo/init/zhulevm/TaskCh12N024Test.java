package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N024.resArr1;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N024.resArr2;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 01.02.2016.
 */
public class TaskCh12N024Test {


    public static void main(String arg[]) {

        int[][] arrayA = new int[][]{
                {1, 1, 1, 1, 1, 1},

                {1, 2, 3, 4, 5, 6},

                {1, 3, 6, 10, 15, 21},

                {1, 4, 10, 20, 35, 56},

                {1, 5, 15, 35, 70, 126},

                {1, 6, 21, 56, 126, 252}
        };

        int[][] arrayB = new int[][]{
                {1, 2, 3, 4, 5, 6},

                {2, 3, 4, 5, 6, 1},

                {3, 4, 5, 6, 1, 2},

                {4, 5, 6, 1, 2, 3},

                {5, 6, 1, 2, 3, 4},

                {6, 1, 2, 3, 4, 5},
        };

        int a[][] = new int[6][6];
        int b[][] = new int[6][6];

        testEq1(arrayA, a);
        testEq2(arrayB, b);
    }

    public static void testEq1(int arrayA[][], int s[][]) {
        assertEquals("TaskCh12N024Test.testEq1: ", arrayA, resArr1(s));
    }

    public static void testEq2(int arrayB[][], int s[][]) {
        assertEquals("TaskCh12N024Test.testEq2: ", arrayB, resArr2(s));
    }
}
