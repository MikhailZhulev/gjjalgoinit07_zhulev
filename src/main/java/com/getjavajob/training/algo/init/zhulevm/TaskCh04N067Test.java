package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh04N067.dayWeek;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 19.01.2016.
 */
public class TaskCh04N067Test {

    public static void main(String arg[]) {
        test1();
        test2();
    }

    public static void test1() {
        assertEquals("TaskCh04N067Test.test 5", "Workday", dayWeek(5));
    }

    public static void test2() {
        assertEquals("TaskCh04N067Test.test 7", "Weekend", dayWeek(7));
    }

}
