package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh11N245.sort;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 31.01.2016.
 */
public class TaskCh11N245Test {

    public static void main(String arg[]) {
        int a[] = {4, -8, 4, -67, 32, -4, 6, -3, 3, -6};
        int b[] = {-8, -67, -4, -3, -6, 3, 6, 32, 4, 4};
        testRes(a, b);
    }

    public static void testRes(int a[], int b[]) {
        assertEquals("TaskCh11N245Test.testSort: ", b, sort(a));
    }
}
