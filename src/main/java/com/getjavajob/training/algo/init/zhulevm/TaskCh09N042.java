package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N042 {

    public static void main(String arg[]) {
        String res = reform("Hello");
        System.out.println(res);
    }
    //method move on one letter and return new words
    public static String reform(String ar) {
        StringBuilder s = new StringBuilder(ar);
        StringBuilder word = new StringBuilder(ar);
        for (int i = 0; i < s.length() - 1; i++) {
            word.setCharAt((i + 1), s.charAt(i));
        }
        char F = s.charAt((s.length() - 1));
        word.setCharAt(0, F);
        String wor = word.toString();
        return wor;
    }
}
