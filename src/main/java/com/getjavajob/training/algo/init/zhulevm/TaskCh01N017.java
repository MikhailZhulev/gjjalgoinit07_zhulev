package com.getjavajob.training.algo.init.zhulevm;

/**
 * Created by ws_1 on 14.01.2016.
 */
public class TaskCh01N017 {

    public static void main(String arg[]) {

        double x = 1;   // value var x
        double a = 1;   // value var a
        double b = 1;   // value var b
        double c = 1;   // value var c

        double resultO = methodsO(x);
        double resultP = methodsP(x, a, b, c);
        double resultR = methodsR(x);
        double resultS = methodsS(x);

        System.out.println("Task O) " + resultO);
        System.out.println("Task P) " + resultP);
        System.out.println("Task R) " + resultR);
        System.out.println("Task S) " + resultS);
    }

    static double methodsO(double x) {
        double q = Math.sqrt(1 - Math.pow(Math.sin(x), 2));
        return q;
    }

    static double methodsP(double x, double a, double b, double c) {
        double q = 1 / (Math.sqrt(a * Math.pow(x, 2) + (b * x) + c));
        return q;
    }

    static double methodsR(double x) {
        double q = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        return q;
    }

    static double methodsS(double x) {
        double q = (Math.abs(x)) + (Math.abs(x + 1));
        return q;
    }

}
