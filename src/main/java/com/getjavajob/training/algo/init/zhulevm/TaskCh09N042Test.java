package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh09N042.reform;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 24.01.2016.
 */
public class TaskCh09N042Test {

    public static void main(String arg[]) {
        testReform();
    }

    public static void testReform() {
        assertEquals("TaskCh09N042Test.testChange: ", "oHell", reform("Hello"));
    }
}
