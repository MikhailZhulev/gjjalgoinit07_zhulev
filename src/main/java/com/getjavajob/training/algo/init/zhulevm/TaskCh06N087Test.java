package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh06N087.res;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 22.01.2016.
 */
public class TaskCh06N087Test {

    public static void main(String arg[]) {

        testEquals();
    }

    public static void testEquals() {
        assertEquals("TaskCh05N038Test.testEquals 5 5: ", "Win Friends", res(5, 5));
        assertEquals("TaskCh05N038Test.testEquals 2 5: ", "Win 2 team", res(2, 5));
        assertEquals("TaskCh05N038Test.testEquals 5 2: ", "Win 1 team", res(5, 2));
    }
}
