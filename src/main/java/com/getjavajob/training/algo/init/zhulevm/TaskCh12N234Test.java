package com.getjavajob.training.algo.init.zhulevm;

import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N234.deleteColumnArray;
import static com.getjavajob.training.algo.init.zhulevm.TaskCh12N234.deleteStringArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ws_1 on 07.02.2016.
 */
public class TaskCh12N234Test {

    public static void main(String arg[]) {

        int[][] realArray = new int[][]{{16, 22, 22, 15, 16, 27, 22}, {18, 26, 20, 18, 19, 27, 24}, {23, 17, 22, 27, 29, 23, 27}, {22, 20, 28, 25, 22, 16, 17}, {25, 26, 27, 24, 16, 28, 17}, {27, 22, 19, 23, 18, 21, 25}, {20, 27, 21, 21, 25, 18, 27}};

        int[][] deleteString = new int[][]{{16, 22, 22, 15, 16, 27, 22}, {18, 26, 20, 18, 19, 27, 24}, {22, 20, 28, 25, 22, 16, 17}, {25, 26, 27, 24, 16, 28, 17}, {27, 22, 19, 23, 18, 21, 25}, {20, 27, 21, 21, 25, 18, 27}, {0, 0, 0, 0, 0, 0, 0}};
        int[][] deleteColumn = new int[][]{{16, 22, 15, 16, 27, 22, 0}, {18, 26, 18, 19, 27, 24, 0}, {23, 17, 27, 29, 23, 27, 0}, {22, 20, 25, 22, 16, 17, 0}, {25, 26, 24, 16, 28, 17, 0}, {27, 22, 23, 18, 21, 25, 0}, {20, 27, 21, 25, 18, 27, 0}};

        deleteArr(deleteString, deleteColumn, realArray);
    }

    public static void deleteArr(int[][] deleteString, int[][] deleteColumn, int realArray[][]) {

        assertEquals("TaskCh12N234Test.deleteColumn: ", deleteColumn, deleteColumnArray(realArray, 3));
        assertEquals("TaskCh12N234Test.deleteString: ", deleteString, deleteStringArray(realArray, 3));
    }

}
